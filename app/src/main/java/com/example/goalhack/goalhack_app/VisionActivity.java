package com.example.goalhack.goalhack_app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class VisionActivity extends AppCompatActivity {
    public TextView description ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vision);

        description = (TextView)findViewById(R.id.descriptionId);

        description.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(VisionActivity.this,Dashboard.class);
                startActivity(intent);

            }
        });
    }
}
